FROM picoded/ubuntu-openjdk-8-jdk

# install bases for Ubuntu
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y wget && \
    apt-get install -y zip unzip

# install gradle
ENV GRADLE_VERSION="6.6.1"
ENV GRADLE_HOME="/opt/gradle/gradle-${GRADLE_VERSION}"
ENV PATH="${GRADLE_HOME}/bin:${PATH}"
RUN wget https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip -P /tmp && \
    unzip -d /opt/gradle /tmp/gradle-*.zip && \
    rm /tmp/gradle-*.zip

# install kotlin
ENV PATH="$PATH:/opt/kotlin/kotlinc/bin"
RUN wget https://github.com/JetBrains/kotlin/releases/download/v1.4.10/kotlin-compiler-1.4.10.zip -P /tmp && \
    unzip -d /opt/kotlin /tmp/kotlin-compiler-*.zip && \
    rm /tmp/kotlin-compiler-*.zip


CMD ["bash"]
