# build-kotlin

## image

Kotlin docker image.

The project builds the image and push it to docker-hub. Two images are pushed: 
- tagged version: thanks to DOCKER_IMAGE_VERSION parameter
- latest version

## docker-hub
This image is available on docker-hub:
https://hub.docker.com/r/balacsoft/build-kotlin

To pull the image:
docker pull balacsoft/build-kotlin

## content

- kotlin 1.4.10
- gradle 6.6.1
